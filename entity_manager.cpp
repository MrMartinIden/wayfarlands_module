#include "entity_manager.h"

#include <core/os/input.h>

#include <core/engine.h>
#include <scene/3d/camera.h>
#include <scene/3d/immediate_geometry.h>
#include <scene/3d/mesh_instance.h>
#include <scene/main/viewport.h>
#include <scene/resources/mesh_data_tool.h>

#include "entity_node.h"
#include "height_map.h"

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <sstream>
#include <vector>

#include "glm/gtx/string_cast.hpp"

template <typename T>
bool is_inside_global_volume(T &collider)
{
    return globalAABB.is_inside(collider.getAABB());
}

Vector3 toVec(const glm::vec3 &v) {
	return { v.x, v.y, v.z };
}

glm::vec3 toVec(const Vector3 &v) {
	return { v.x, v.y, v.z };
}

Quat toQuat(const glm::quat &q)
{
    return { q.x, q.y, q.z, q.w };
}

void EntityManager::_bind_methods() {
	/*
    register_method("_process", &EntityManager::_process);
    register_method("_ready", &EntityManager::_ready);
	*/
}

EntityManager::EntityManager() :
		m_client(m_registry) {
	_init();
}

EntityManager::~EntityManager() {}

void EntityManager::_init()
{
    //create_floor();

    drawAABB(globalAABB);
}

void EntityManager::_notification(int p_what) {
	switch (p_what) {

		case NOTIFICATION_ENTER_TREE:
			set_process(true);
			break;

		case NOTIFICATION_ENTER_WORLD:
			break;

		case NOTIFICATION_EXIT_WORLD:
			break;

		case NOTIFICATION_TRANSFORM_CHANGED:
			break;

		case NOTIFICATION_VISIBILITY_CHANGED:
			break;

		case NOTIFICATION_PROCESS:
			auto time = get_process_delta_time();
			_process(time);
			break;
	}
}

void EntityManager::replace_nodes()
{
    if (!is_inside_tree())
    {
        return;
    }

    size_t child_count = this->get_child_count();

    if (child_count > 0)
    {
        for (decltype(child_count) i = 0; i < child_count; ++i)
        {
            EntityNode *node = Object::cast_to<EntityNode>(this->get_child(i));
            if (node != nullptr)
            {
                const auto entity = m_registry.create();
                Transform transform = node->get_global_transform();
                auto local_position = transform.get_origin();
                auto offset = node->get_offset();
                m_registry.emplace<Transform>(entity, transform);

                if (node->get_type() == static_cast<uint8_t>(Type::Capsule))
                {
                    spawn_player(local_position, offset);
                }
                else if (node->get_type() == static_cast<uint8_t>(Type::Box))
                {
                    create_floor(local_position, offset);
                }
                else if (node->get_type() == static_cast<uint8_t>(Type::Triangle))
                {
                    glm::quat rotation = glm::identity<glm::quat>();
                    auto position = toVec(local_position);

                    std::array<glm::vec3, 3> vertices{};
                    //                    vertices[0] = glm::vec3{ 1, 1, 15 };
                    //                    vertices[1] = glm::vec3{ 1, 4, 1 };
                    //                    vertices[2] = glm::vec3{ 15, 1, 1 };

                    vertices[0] = glm::vec3(58.000000, 96.621262, 62.000000) - position;
                    vertices[1] = glm::vec3(58.500000, 96.716721, 62.000000) - position;
                    vertices[2] = glm::vec3(58.000000, 96.678955, 62.500000) - position;

                    WFL::Triangle triangle{ vertices };

                    create_triangle(triangle);
                }
            }
        }
    }
}

void EntityManager::remove_entities()
{
    auto view = m_registry.view<Spatial *>();

    view.each([&](Spatial *node) {
        this->remove_child(node);
    });

    m_registry.clear();
}

void EntityManager::getTriangleMesh()
{
    Node *mesh = get_node_or_null(String("/root/Spatial/StaticMesh/Mesh"));

	mesh != nullptr ? print_line("mesh is valid") : print_line("mesh is invalid");

	MeshInstance *meshInstance = cast_to<MeshInstance>(mesh);

	meshInstance != nullptr ? print_line("meshInstance is valid") : print_line("meshInstance is invalid");

	MeshDataTool *mdt = memnew(MeshDataTool);

	Ref<ArrayMesh> godot_mesh = meshInstance->get_mesh();
	mdt->create_from_surface(godot_mesh, 0);

	size_t count = mdt->get_vertex_count();

	std::vector<glm::vec3> vertices(count);

	Vector3 translation = meshInstance->get_global_transform().get_origin();
	Quat quat = meshInstance->get_global_transform().get_basis().operator Quat();

	for (decltype(count) i = 0; i < count; ++i) {
		Vector3 v = mdt->get_vertex(i);
		vertices[i] = toVec(v);
	}

	glm::vec3 p = toVec(translation);
	glm::quat q{ quat.w, quat.x, quat.y, quat.z };

	WFL::Convex convex{ p, q, std::move(vertices) };

    create_triangle_mesh(convex);
}

void EntityManager::get_terrain()
{
    size_t child_count = this->get_child_count();

    if (child_count > 0)
    {
        for (size_t i{ 0 }; i < child_count; ++i)
        {
            HeightMap *height_map = Object::cast_to<HeightMap>(this->get_child(i));
            if (height_map == nullptr)
            {
                continue;
            }

            print_line(String("Terrain is getting."));

            auto &terrainCollision = m_client.get_physics_system().m_terrainCollision;

            terrainCollision.m_position = toVec(height_map->get_global_transform().get_origin());
            terrainCollision.m_resolution = height_map->get_data()->get_resolution();

            terrainCollision.m_height_map = height_map->get_data()->m_heights;
            assert(!terrainCollision.m_height_map.empty());
            break;
        }
    }
}

void EntityManager::_ready() {

    /*
    

    getTriangleMesh();

    getStone();

    // glm::vec3 v = ;

    // std::cout << glm::to_string(glm::quat{}) << std::endl;

    // assert(false);
    */
}

void EntityManager::getStone() {
	Node *mesh = get_node_or_null(String("/root/Spatial/Stone"));

	mesh != nullptr ? print_line("stone is valid") : print_line("stone is invalid");

	MeshInstance *meshInstance = cast_to<MeshInstance>(mesh);

	meshInstance != nullptr ? print_line("meshInstance is valid") : print_line("meshInstance is invalid");

	MeshDataTool *mdt = memnew(MeshDataTool);

	Ref<ArrayMesh> godot_mesh = meshInstance->get_mesh();
	mdt->create_from_surface(godot_mesh, 0);

	auto translation = meshInstance->get_global_transform().get_origin();
	auto quat =
			meshInstance->get_global_transform().get_basis().operator Quat();

	auto countTri = mdt->get_face_count();

	std::vector<std::array<glm::vec3, 3> > triangles;
	triangles.reserve(countTri);

	for (auto i = 0; i < countTri; ++i) {
		auto v0 = mdt->get_face_vertex(i, 0);
        auto v1 = mdt->get_face_vertex(i, 1);
        auto v2 = mdt->get_face_vertex(i, 2);

        triangles.push_back(std::array<glm::vec3, 3>{ toVec(mdt->get_vertex(v0)), toVec(mdt->get_vertex(v1)), toVec(mdt->get_vertex(v2)) });
	}

	glm::vec3 p = toVec(translation);
    glm::quat q{ quat.w, quat.x, quat.y, quat.z };

    std::string inputfile("Stone.obj");

    WFL::TriangleMesh triMesh{ p, q, std::move(triangles) };

    create_triangle_mesh(triMesh);

    /*
    Convex convex{p, q, std::move(vertices)};

    create_triangle_mesh(convex);
    */

    // auto v = convex.support(glm::vec3{-1, -1, -1});

    // Vector3 center = Vector3{v.x, v.y, v.z};

    // BoundVolume(convex.aabb_center(), convex.m_max - convex.aabb_center());
}

void EntityManager::input()
{
    if (is_inside_tree() && !Engine::get_singleton()->is_editor_hint())
    {
        Input *input = Input::get_singleton();

        auto view = m_registry.view<WFL::Direction>();

        view.each([input](WFL::Direction &dir) {
            dir = { 0, 0, 0 };

            if (input->is_action_pressed("up"))
            {
                dir.z = 1;
            }

            if (input->is_action_pressed("down"))
            {

                dir.z = -1;
            }

            if (input->is_action_pressed("left"))
            {
                dir.x = -1;
            }

            if (input->is_action_pressed("right"))
            {
                dir.x = 1;
            }

            if (input->is_action_pressed("jump"))
            {
                //dir.m_up = 1;
            }
        });

        if (input->is_action_just_pressed("spawn"))
        {
            this->spawn_player(Vector3{ 10, 110.f, 10.f });
        }
    }
}

void EntityManager::_process([[maybe_unused]] float dt)
{
    camera_look_at();

    input();

    m_client.update(m_registry);

    auto view = m_registry.view<WFL::CapsuleCollider, Spatial *>();

    for (auto entity : view)
    {
        WFL::CapsuleCollider &capsule = view.get<WFL::CapsuleCollider>(entity);

        Spatial *s_entity = view.get<Spatial *>(entity);

        assert(is_inside_global_volume(capsule));
        s_entity->set_translation(toVec(capsule.m_position));
    }
}

void EntityManager::create_triangle(const WFL::Triangle &triangle)
{
    print_line("Adding triangle");
    entt::entity entity = m_registry.create();
    Spatial *entity_node = memnew(Spatial);
    this->add_child(entity_node);
    entity_node->set_translation(toVec(triangle.m_position));
    m_registry.emplace<Spatial *>(entity, entity_node);
    // print_line("Floor Added");

    m_registry.emplace<WFL::MortonPrimitive>(entity);
    m_registry.emplace<WFL::Triangle>(entity, triangle);
    m_registry.emplace<WFL::AABB>(entity, triangle.getAABB());
    //m_registry.emplace<WFL::CollisionGroup>(entity, WFL::CollisionGroup::Static);

    m_client.get_physics_system().m_broadPhase.prepare(m_registry);

    drawTriangle(triangle);
    drawAABB(triangle.getAABB(), Color{ 0, 1, 0 });

    print_line("Triangle Added");
}

void EntityManager::create_triangle_mesh(WFL::TriangleMesh &mesh)
{
    print_line("Adding stone");
    const entt::entity entity = m_registry.create();
    Spatial *entity_node = memnew(Spatial);
    this->add_child(entity_node);
    entity_node->set_translation(Vector3{ 0, 0, 0 });
    m_registry.emplace<Spatial *>(entity, entity_node);
    print_line("Stone Added");

    m_registry.emplace<WFL::MortonPrimitive>(entity);
    m_registry.emplace<WFL::TriangleMesh>(entity, mesh);
    m_registry.emplace<WFL::AABB>(entity, mesh.getAABB());
    //m_registry.emplace<WFL::CollisionGroup>(entity, WFL::CollisionGroup::Static);

    WFL::AABB aabb = mesh.getAABB();

    print_line(glm::to_string(aabb.min()).c_str());
    print_line(glm::to_string(aabb.max()).c_str());

    //BoundVolume(mesh.getAABB());
}

void EntityManager::create_triangle_mesh(WFL::Convex &convex) {
	// print_line("Adding floor");
	entt::entity entity = m_registry.create();
    Spatial *entity_node = memnew(Spatial);
    this->add_child(entity_node);
    entity_node->set_translation(Vector3{ 0, 0, 0 });
    m_registry.emplace<Spatial *>(entity, entity_node);
    // print_line("Floor Added");

    m_registry.emplace<WFL::MortonPrimitive>(entity);
    m_registry.emplace<WFL::Convex>(entity, convex);
    m_registry.emplace<WFL::AABB>(entity, convex.getAABB());
    //m_registry.emplace<WFL::CollisionGroup>(entity, WFL::CollisionGroup::Static);

    //BoundVolume(convex.getAABB());
}

void EntityManager::create_obb() {
	entt::entity entity = m_registry.create();
	Spatial *entity_node = memnew(Spatial);
	this->add_child(entity_node);
	Vector3 pos{ 0, 10, 0 };
	m_registry.emplace<Spatial *>(entity, entity_node);
	// print_line("Floor Added");

	glm::vec3 extent{ 50, 1, 50 };

    glm::vec3 position = glm::vec3{ pos.x, pos.y, pos.z };
    glm::quat rotation{ glm::vec3{ 1, 0, 0.3f } };

    std::cout << glm::to_string(rotation) << std::endl;

    // assert(false);

    WFL::BoxCollider box{ position, rotation, extent };

    m_registry.emplace<WFL::BoxCollider>(entity, box);
    m_registry.emplace<WFL::AABB>(entity, box.getAABB());
    //m_registry.emplace<WFL::CollisionGroup>(entity, WFL::CollisionGroup::Static);

    // BoundVolume(box.GetAABB());
    //BoundVolume(box.support(glm::vec3{ 1, 1, 1 }));
}

void EntityManager::create_floor(const Vector3 &pos, uint32_t p_offset)
{
    print_line("Adding floor");

    glm::vec3 extent{ 10, 1, 10 };

    Spatial *entity_node = memnew(Spatial);
    MeshInstance *instance = memnew(MeshInstance);
    CubeMesh *cube = memnew(CubeMesh);
    cube->set_size(toVec(extent) * 2);
    instance->set_mesh(cube);
    entity_node->add_child(instance);
    this->add_child(entity_node);

    glm::quat rotation = glm::identity<glm::quat>();

    glm::vec3 position = toVec(pos);

#if 0
    entt::entity entity = m_registry.create();

    glm::vec3 position = toVec(pos);
    

    WFL::BoxCollider box{ position, rotation, extent };

    entity_node->set_rotation(toVec(glm::eulerAngles(box.m_rotation)));
    assert(is_inside_global_volume(box));
    

    m_registry.emplace<WFL::MortonPrimitive>(entity);
    m_registry.emplace<WFL::BoxCollider>(entity, box);
    m_registry.emplace<WFL::AABB>(entity, box.getAABB());
    //m_registry.emplace<WFL::CollisionGroup>(entity, WFL::CollisionGroup::Static);
#else

    //auto [local_pos, offset] = WFL::to_local(position);
    //WFL::Offset2::encode(offset[0], offset[1])
    auto entity = WFL::spawn_static_box(m_registry, p_offset, position, rotation, extent);
    entity_node->set_translation(pos);
#endif

    m_registry.emplace<Spatial *>(entity, entity_node);

    m_client.get_physics_system().m_broadPhase.prepare(m_registry);

    //drawAABB(box.getAABB());

    print_line("Floor Added");
}

void EntityManager::camera_look_at()
{

    if (m_entity_player == entt::null)
    {
        return;
    }

    Viewport *viewport = get_viewport();
    if (viewport != nullptr)
    {
        Camera *camera = viewport->get_camera();
        if (camera != nullptr)
        {
            auto &capsule = m_registry.get<WFL::CapsuleCollider>(m_entity_player);

            auto pos = capsule.m_position;

            auto position = toVec(pos);
            camera->look_at(position, Vector3{ 0, 1, 0 });
        }
    }
}

template <typename T>
void EntityManager::spawn_player(T &&p_pos, uint32_t p_offset)
{
    print_line("Adding capsule");

    auto pos = std::forward<T>(p_pos);

    float radius = 0.5f;
    float halfHeight = 1.f;

    Spatial *entity_node = memnew(Spatial);
    MeshInstance *instance = memnew(MeshInstance);
    CapsuleMesh *capsule = memnew(CapsuleMesh);
    capsule->set_radius(radius);
    capsule->set_mid_height(halfHeight);
    instance->set_mesh(capsule);
    instance->set_rotation_degrees(Vector3(90.0, 0, 0));
    entity_node->add_child(instance);
    this->add_child(entity_node);

#if 0

    entt::entity entity = m_registry.create();

    {
		glm::vec3 position = toVec(pos);
        glm::quat rot = glm::identity<glm::quat>();

        WFL::CapsuleCollider capsule{ position, rot, radius, halfHeight - radius };

        m_registry.emplace<WFL::Direction>(entity);
        //m_registry.emplace<WFL::Pair>(entity);
        m_registry.emplace<WFL::MortonPrimitive>(entity);
        m_registry.emplace<WFL::MovementComponent>(entity, 0, 0);

        //m_registry.emplace<WFL::Velocity>(entity);
        m_registry.emplace<WFL::CapsuleCollider>(entity, capsule);
        m_registry.emplace<WFL::AABB>(entity, capsule.getAABB());

        //m_registry.emplace<WFL::CollisionGroup>(entity, WFL::CollisionGroup::Character);

        //drawAABB(capsule.getAABB(), Color{ 1, 0, 0 }, entity_node);
    }

#else
    glm::vec3 position = toVec(pos);
    //auto [local_pos, offset] = WFL::to_local(position);
    //auto local_offset = WFL::Offset2::encode(offset[0], offset[1]);
    m_entity_player = WFL::spawn_capsule(m_registry, p_offset, position);

    entity_node->set_translation(pos);
#endif

    m_registry.emplace<Spatial *>(m_entity_player, entity_node);

    m_client.get_physics_system().m_broadPhase.prepare(m_registry);

    get_terrain();

    print_line("Capsule Added");
}

void EntityManager::drawAABB(const WFL::AABB &aabb, Color p_color, Spatial *parent)
{
    std::array<Vector3, 8> vertices;

    ImmediateGeometry *immediateGeom = memnew(ImmediateGeometry);

    SpatialMaterial *line_material = memnew(SpatialMaterial);
    line_material->set_flag(SpatialMaterial::FLAG_UNSHADED, true);
    line_material->set_line_width(3.0);
    line_material->set_feature(SpatialMaterial::FEATURE_TRANSPARENT, true);
    line_material->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR,
            true);
    line_material->set_flag(SpatialMaterial::FLAG_SRGB_VERTEX_COLOR, true);
    line_material->set_albedo(Color(1, 1, 1));

    immediateGeom->set_material_override(line_material);

    immediateGeom->begin(Mesh::PRIMITIVE_LINES, Ref<Texture>());
    immediateGeom->set_color(p_color);

    if (parent != nullptr)
    {
        parent->add_child(immediateGeom);

        Vector3 v = toVec(aabb.max());

        Vector3 translation = parent->get_translation();

        {
			// top
			vertices[0] = v - translation;

			v.z = aabb.min().z;
			vertices[1] = v - translation;

			immediateGeom->add_vertex(vertices[0]);
			immediateGeom->add_vertex(vertices[1]);

			v.x = aabb.min().x;
			vertices[2] = v - translation;

			immediateGeom->add_vertex(vertices[1]);
			immediateGeom->add_vertex(vertices[2]);

			v.z = aabb.max().z;
			vertices[3] = v - translation;

			immediateGeom->add_vertex(vertices[2]);
			immediateGeom->add_vertex(vertices[3]);

			immediateGeom->add_vertex(vertices[3]);
			immediateGeom->add_vertex(vertices[0]);

			// bottom
			v = vertices[0];
			v.y = aabb.min().y;

			vertices[4] = v - translation;

			v.z = aabb.min().z;
			vertices[5] = v - translation;

			immediateGeom->add_vertex(vertices[4]);
			immediateGeom->add_vertex(vertices[5]);

			v.x = aabb.min().x;
			vertices[6] = v - translation;

			immediateGeom->add_vertex(vertices[5]);
			immediateGeom->add_vertex(vertices[6]);

			v.z = aabb.max().z;
			vertices[7] = v - translation;

			immediateGeom->add_vertex(vertices[6]);
			immediateGeom->add_vertex(vertices[7]);

			immediateGeom->add_vertex(vertices[7]);
			immediateGeom->add_vertex(vertices[4]);
		}

		{
			for (uint8_t i = 0; i < 4; ++i) {
				immediateGeom->add_vertex(vertices[i]);
				immediateGeom->add_vertex(vertices[i + 4]);
			}
		}

        immediateGeom->end();
    }
    else
    {
        add_child(immediateGeom);

        Vector3 v = toVec(aabb.max());

		{
			// top
			vertices[0] = v;

			v.z = aabb.min().z;
			vertices[1] = v;

			immediateGeom->add_vertex(vertices[0]);
			immediateGeom->add_vertex(vertices[1]);

			v.x = aabb.min().x;
			vertices[2] = v;

			immediateGeom->add_vertex(vertices[1]);
			immediateGeom->add_vertex(vertices[2]);

			v.z = aabb.max().z;
			vertices[3] = v;

			immediateGeom->add_vertex(vertices[2]);
			immediateGeom->add_vertex(vertices[3]);

			immediateGeom->add_vertex(vertices[3]);
			immediateGeom->add_vertex(vertices[0]);

			// bottom
			v = vertices[0];
			v.y = aabb.min().y;

			vertices[4] = v;

			v.z = aabb.min().z;
			vertices[5] = v;

			immediateGeom->add_vertex(vertices[4]);
			immediateGeom->add_vertex(vertices[5]);

			v.x = aabb.min().x;
			vertices[6] = v;

			immediateGeom->add_vertex(vertices[5]);
			immediateGeom->add_vertex(vertices[6]);

			v.z = aabb.max().z;
			vertices[7] = v;

			immediateGeom->add_vertex(vertices[6]);
			immediateGeom->add_vertex(vertices[7]);

			immediateGeom->add_vertex(vertices[7]);
			immediateGeom->add_vertex(vertices[4]);
		}

		{
			for (uint8_t i = 0; i < 4; ++i) {
				immediateGeom->add_vertex(vertices[i]);
				immediateGeom->add_vertex(vertices[i + 4]);
			}
		}

        immediateGeom->end();
    }
}

void EntityManager::drawTriangle(const WFL::Triangle &triangle)
{
    ImmediateGeometry *immediateGeom = memnew(ImmediateGeometry);

    add_child(immediateGeom);

    SpatialMaterial *line_material = memnew(SpatialMaterial);
    line_material->set_flag(SpatialMaterial::FLAG_UNSHADED, true);
    line_material->set_line_width(3.0);
    line_material->set_feature(SpatialMaterial::FEATURE_TRANSPARENT, true);
    line_material->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR, true);
    line_material->set_flag(SpatialMaterial::FLAG_SRGB_VERTEX_COLOR, true);
    line_material->set_albedo(Color(1, 1, 1));

    immediateGeom->set_material_override(line_material);

    immediateGeom->begin(Mesh::PRIMITIVE_LINES);
    immediateGeom->set_color(Color{ 1, 0, 0 });

    {
        immediateGeom->add_vertex(toVec(triangle.m_vertices[0] + triangle.m_position));
        immediateGeom->add_vertex(toVec(triangle.m_vertices[1] + triangle.m_position));

        immediateGeom->add_vertex(toVec(triangle.m_vertices[1] + triangle.m_position));
        immediateGeom->add_vertex(toVec(triangle.m_vertices[2] + triangle.m_position));

        immediateGeom->add_vertex(toVec(triangle.m_vertices[2] + triangle.m_position));
        immediateGeom->add_vertex(toVec(triangle.m_vertices[0] + triangle.m_position));
    }

    immediateGeom->end();
}

void EntityManager::BoundVolume(glm::vec3 center, [[maybe_unused]] glm::vec3 extent) {
	std::array<Vector3, 8> vertices;

	ImmediateGeometry *immediateGeom = memnew(ImmediateGeometry);

	add_child(immediateGeom);

	SpatialMaterial *line_material = memnew(SpatialMaterial);
	line_material->set_flag(SpatialMaterial::FLAG_UNSHADED, true);
	line_material->set_line_width(3.0);
    line_material->set_feature(SpatialMaterial::FEATURE_TRANSPARENT, true);
    line_material->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR,
            true);
    line_material->set_flag(SpatialMaterial::FLAG_SRGB_VERTEX_COLOR, true);
    line_material->set_albedo(Color(1, 1, 1));

    immediateGeom->set_material_override(line_material);

    immediateGeom->begin(Mesh::PRIMITIVE_LINES, Ref<Texture>());
    immediateGeom->set_color(Color{ 1, 0, 0 });

    Vector3 v = toVec(center);

    /*
    Vector3 m_extent{extent.x, extent.y, extent.z};

    {
        v.x -= m_extent.x;
        v.y += m_extent.y;
        v.z -= m_extent.z;

        // top
        vertices[0] = v;

        v += Vector3{0, 0, m_extent.z * 2};
        vertices[1] = v;

        immediateGeom->add_vertex(vertices[0]);
        immediateGeom->add_vertex(vertices[1]);

        v += Vector3{m_extent.x * 2, 0, 0};
        vertices[2] = v;

        immediateGeom->add_vertex(vertices[1]);
        immediateGeom->add_vertex(vertices[2]);

        v += Vector3{0, 0, -m_extent.z * 2};
        vertices[3] = v;

        immediateGeom->add_vertex(vertices[2]);
        immediateGeom->add_vertex(vertices[3]);

        immediateGeom->add_vertex(vertices[3]);
        immediateGeom->add_vertex(vertices[0]);

        // bottom
        v = vertices[0] + Vector3{0, -m_extent.y * 2, 0};
        vertices[4] = v;

        v += Vector3{0, 0, m_extent.z * 2};
        vertices[5] = v;

        immediateGeom->add_vertex(vertices[4]);
        immediateGeom->add_vertex(vertices[5]);

        v += Vector3{m_extent.x * 2, 0, 0};
        vertices[6] = v;

        immediateGeom->add_vertex(vertices[5]);
        immediateGeom->add_vertex(vertices[6]);

        v += Vector3{0, 0, -m_extent.z * 2};
        vertices[7] = v;

        immediateGeom->add_vertex(vertices[6]);
        immediateGeom->add_vertex(vertices[7]);

        immediateGeom->add_vertex(vertices[7]);
        immediateGeom->add_vertex(vertices[4]);
    }

    {
        for (uint8_t i = 0; i < 4; ++i)
        {
            immediateGeom->add_vertex(vertices[i]);
            immediateGeom->add_vertex(vertices[i + 4]);
        }
    }
    */

    immediateGeom->add_vertex(v);
    immediateGeom->add_vertex(v + Vector3{ 0, 10, 0 });

    immediateGeom->end();
}
