#include "register_types.h"

#include "entity_manager.h"
#include "entity_manager_editor_plugin.h"
#include "entity_node.h"

#include "height_map.h"

void register_wayfarlands_types() {
#ifndef _3D_DISABLED
    ClassDB::register_class<EntityManager>();
    ClassDB::register_class<EntityNode>();

    ClassDB::register_class<HeightMap>();
    ClassDB::register_class<HeightMapData>();

#ifdef TOOLS_ENABLED
    EditorPlugins::add_by_type<EntityManagerEditorPlugin>();
#endif

#endif
}

void unregister_wayfarlands_types() {
}
