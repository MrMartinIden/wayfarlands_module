#include "entity_node.h"

#include "scene/3d/mesh_instance.h"

EntityNode::EntityNode()
{
    m_mesh = memnew(MeshInstance);
    add_child(m_mesh);
}

void EntityNode::set_mesh(Ref<Mesh> p_mesh)
{

    if (p_mesh == m_mesh->get_mesh())
    {
        return;
    }

    m_mesh->set_mesh(p_mesh);
}

Ref<ArrayMesh> EntityNode::get_mesh()
{
    return m_mesh->get_mesh();
}

void EntityNode::set_type(uint8_t p_type)
{
    if (m_type == static_cast<Type>(p_type))
    {
        return;
    }

    m_type = static_cast<Type>(p_type);
}

void EntityNode::set_offset(uint32_t p_offset)
{
    if (m_offset != p_offset)
    {
        m_offset = p_offset;
    }
}

void EntityNode::_bind_methods()
{

    ClassDB::bind_method(D_METHOD("set_mesh", "p_mesh"), &EntityNode::set_mesh);
    ClassDB::bind_method(D_METHOD("get_mesh"), &EntityNode::get_mesh);

    ClassDB::add_property(get_class_static(),
            PropertyInfo(Variant::OBJECT, "mesh"),
            _scs_create("set_mesh"),
            _scs_create("get_mesh"));

    ClassDB::bind_method(D_METHOD("set_type", "p_type"), &EntityNode::set_type);
    ClassDB::bind_method(D_METHOD("get_type"), &EntityNode::get_type);

    ClassDB::add_property(get_class_static(),
            PropertyInfo(Variant::INT, "type", PROPERTY_HINT_ENUM, "None, Capsule, Box, Triangle, Triangle Mesh"),
            _scs_create("set_type"),
            _scs_create("get_type"));

    ClassDB::bind_method(D_METHOD("set_offset", "p_offset"), &EntityNode::set_offset);
    ClassDB::bind_method(D_METHOD("get_offset"), &EntityNode::get_offset);

    ClassDB::add_property(get_class_static(),
            PropertyInfo(Variant::INT, "offset", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_EDITOR),
            _scs_create("set_offset"),
            _scs_create("get_offset"));
}
