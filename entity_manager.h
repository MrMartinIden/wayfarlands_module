#pragma once

#include <scene/3d/spatial.h>

#include <scene/resources/primitive_meshes.h>

#include <entt/entt.hpp>

#include <Client.h>

#include "iosfwd"

static constexpr WFL::AABB globalAABB = { glm::vec3{ 0 }, glm::vec3{ 4096, 256, 4096 } };

namespace WFL
{
    struct Convex;
    struct AABB;
} // namespace WFL

class MeshInstance;

class EntityManager : public Spatial {
	GDCLASS(EntityManager, Spatial)

public:
	static void _bind_methods();

	void _notification(int p_what);

	EntityManager();
	virtual ~EntityManager();

    void _init();
    void _process(float delta);
    void _ready();

    void input();

    void getTriangleMesh();

    void getStone();

    void replace_nodes();

    void remove_entities();

    void get_terrain();

    void camera_look_at();

    template <typename T>
    void spawn_player(T &&p_pos, uint32_t p_offset = 0);

    void create_obb();

    void create_floor(const Vector3 &p_pos, uint32_t p_offset = 0);

    void create_triangle(const WFL::Triangle &triangle);

    void create_triangle_mesh(WFL::TriangleMesh &mesh);

    void create_triangle_mesh(WFL::Convex &convex);

    void drawTriangle(const WFL::Triangle &triangle);

    void drawAABB(const WFL::AABB &aabb, Color p_color = { 1, 0, 0 }, Spatial *parent = nullptr);

    void BoundVolume(glm::vec3 v = glm::vec3{}, glm::vec3 extent = glm::vec3{});

private:
    // registry
    entt::registry m_registry;

    entt::entity m_entity_player{ entt::null };

    WFL::Client m_client;

    MeshInstance *m_meshTerrain = nullptr;
};
