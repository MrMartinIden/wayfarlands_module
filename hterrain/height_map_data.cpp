#include "height_map_data.h"

#include <cassert>

HeightMapData::HeightMapData()
{
    m_resolution = 0;
}

void HeightMapData::set_depth(uint8_t p_depth) {
	m_depth = p_depth;
}

void HeightMapData::set_multiplier(uint16_t p_multiplier) {
	m_multiplier = p_multiplier;
}

uint32_t HeightMapData::get_resolution() const
{
    return m_resolution;
}

void HeightMapData::set_resolution(uint32_t p_res)
{

    if (p_res == m_resolution)
    {
        return;
    }

    m_resolution = p_res;
}

struct ImageLockGuard
{
    Ref<Image> m_image;

    ImageLockGuard(Ref<Image> image) :
            m_image(image)
    {
        m_image->lock();
    }

    ~ImageLockGuard()
    {
        m_image->unlock();
    }
};

void HeightMapData::set_texture(Ref<Texture> p_texture)
{
    if (m_texture != p_texture)
    {
        m_texture = p_texture;

        auto image = p_texture->get_data();

        int height = image->get_height();
        int width = image->get_width();

        assert(height == width);
        set_resolution(width);

        m_heights.reserve(height * width);

        {
            ImageLockGuard lock_guard{ image };
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    auto pixel = image->get_pixel(x, y).r;
                    float h = m_multiplier * pixel;

                    m_heights.push_back(h);
                }
            }
        }
    }
}

void HeightMapData::_bind_methods()
{

    ClassDB::bind_method(D_METHOD("set_resolution", "p_res"), &HeightMapData::set_resolution);
    ClassDB::bind_method(D_METHOD("get_resolution"), &HeightMapData::get_resolution);

    ClassDB::bind_method(D_METHOD("set_depth", "p_depth"), &HeightMapData::set_depth);
    ClassDB::bind_method(D_METHOD("get_depth"), &HeightMapData::get_depth);

    ClassDB::bind_method(D_METHOD("set_multiplier", "p_multiplier"), &HeightMapData::set_multiplier);
    ClassDB::bind_method(D_METHOD("get_multiplier"), &HeightMapData::get_multiplier);

    ClassDB::add_property(get_class_static(),
            PropertyInfo(Variant::INT, "resolution", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_EDITOR),
            _scs_create("set_resolution"),
            _scs_create("get_resolution"));

    ClassDB::add_property(get_class_static(),
            PropertyInfo(Variant::INT, "depth", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_EDITOR),
            _scs_create("set_depth"),
            _scs_create("get_depth"));

    ClassDB::add_property(get_class_static(),
            PropertyInfo(Variant::INT, "multiplier", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_EDITOR),
            _scs_create("set_multiplier"),
            _scs_create("get_multiplier"));

    ClassDB::bind_method(D_METHOD("set_texture", "p_texture"), &HeightMapData::set_texture);
    ClassDB::bind_method(D_METHOD("get_texture"), &HeightMapData::get_texture);

    ClassDB::add_property(get_class_static(),
            PropertyInfo(Variant::OBJECT, "texture", PROPERTY_HINT_RESOURCE_TYPE, "Texture"),
            _scs_create("set_texture"),
            _scs_create("get_texture"));
}
