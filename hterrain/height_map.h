#pragma once

#include <scene/3d/spatial.h>

#include <scene/resources/primitive_meshes.h>

#include "direct_multimesh_instance.h"
#include "height_map_data.h"

#include <array>
#include <tuple>

#include <entt/entt.hpp>

using namespace hterrain;

class ImmediateGeometry;

// Heightmap-based 3D terrain
class HeightMap : public Spatial {
	GDCLASS(HeightMap, Spatial)
public:

	HeightMap();
    ~HeightMap();

	void set_data(Ref<HeightMapData> data);
    Ref<HeightMapData> get_data() { return m_data; }

    void draw_triangles();

    Vector3 _manual_viewer_pos;

    void add_chunks();

    template <uint8_t Level>
    struct Lod
    {
    };

    struct OffsetChunk
    {
        uint16_t m_value;
    };

    entt::registry m_registry;

    static constexpr auto chunk_size = 64;
    static constexpr uint8_t m_mesh_cache_size = 7;

    struct Chunk
    {
        float x, z;
    };

    std::array<DirectMultiMeshInstance, m_mesh_cache_size> m_mesh_cache;

    ImmediateGeometry *m_immediateGeom{ nullptr };

protected:
	void _notification(int p_what);

private:
    void _process();

    void set_lod_by_distance(const Vector3 &viewer_pos);

    void set_mesh_by_lod();

    void clear_lods();

    Point2i local_pos_to_cell(Vector3 local_pos) const;

    static void _bind_methods();

    void set_custom_material(Ref<ShaderMaterial> p_material);
    inline Ref<ShaderMaterial> get_custom_material() const { return m_custom_material; }

    Ref<ShaderMaterial> m_custom_material;

private:
    Ref<HeightMapData> m_data;
};
