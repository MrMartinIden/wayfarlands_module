#include "height_map.h"

#include <core/engine.h>
#include <scene/3d/camera.h>
#include <scene/3d/immediate_geometry.h>

#include <cassert>
#include <iostream>

template <std::size_t... Is>
constexpr auto indexSequenceReverse(std::index_sequence<Is...> const &)
        -> decltype(std::index_sequence<sizeof...(Is) - 1U - Is...>{});

template <std::size_t N>
using makeIndexSequenceReverse = decltype(indexSequenceReverse(std::make_index_sequence<N>{}));

namespace detail
{
    template <class F, size_t... I>
    constexpr void for_constexpr_impl(F &&func, std::index_sequence<I...>)
    {
        (func(std::integral_constant<std::size_t, I>{}), ...);
    }
} // namespace detail

enum class Order : uint8_t
{
    AscendingOrder,
    DescendingOrder,
};

template <size_t C, Order order = Order::AscendingOrder, class F>
constexpr void for_constexpr(F &&func)
{

    if constexpr (order == Order::AscendingOrder)
    {
        detail::for_constexpr_impl(std::forward<F>(func), std::make_index_sequence<C>{});
    }
    else
    {
        detail::for_constexpr_impl(std::forward<F>(func), makeIndexSequenceReverse<C>{});
    }
}

template <size_t N>
static constexpr decltype(auto) calc_chunks_offset()
{
    std::array<HeightMap::Chunk, N * N> arr{};

    for (size_t i{ 0 }; i < N * N; ++i)
    {
        arr[i].x = (i % N) * HeightMap::chunk_size;
        arr[i].z = (i / N) * HeightMap::chunk_size;
    }

    return arr;
}

static constexpr auto m_offset_chunks = calc_chunks_offset<(HeightMap::chunk_size - 1)>();

HeightMap::HeightMap() {

	set_notify_transform(true);
}

HeightMap::~HeightMap() {
}

void HeightMap::add_chunks()
{
    Size2 mesh_size{ chunk_size, chunk_size };

    for (uint8_t i = 0; i < m_mesh_cache_size; ++i)
    {
        Ref<PlaneMesh> mesh = memnew(PlaneMesh);
        mesh->set_size(mesh_size);
        auto size = (chunk_size * 2) / (1 << i) - 1;
        mesh->set_subdivide_depth(size);
        mesh->set_subdivide_width(size);

        DirectMultiMeshInstance &chunk = m_mesh_cache[i];
        chunk.set_mesh(mesh);
        chunk.set_scenario(get_world());
    }

    auto len = m_offset_chunks.size();
    for (decltype(len) i{ 0 }; i < len; ++i)
    {
        const auto entity = m_registry.create();
        m_registry.emplace<OffsetChunk>(entity, i);
    }
}

void HeightMap::set_data(Ref<HeightMapData> data)
{

    if (m_data != data)
    {
        m_data = data;
    }

    if (m_data.is_valid())
    {

#ifdef TOOLS_ENABLED
		// This is a small UX improvement so that the user sees a default terrain
		if(is_inside_tree() && Engine::get_singleton()->is_editor_hint()) {
			if(data->get_resolution() == 0) {
            }
		}
#endif
    }
}

void HeightMap::_notification(int p_what)
{
    switch (p_what)
    {

        case NOTIFICATION_ENTER_TREE:
            set_process(true);
            break;

        case NOTIFICATION_ENTER_WORLD:
            add_chunks();
            break;

        case NOTIFICATION_EXIT_WORLD:
            break;

        case NOTIFICATION_TRANSFORM_CHANGED:
            break;

        case NOTIFICATION_VISIBILITY_CHANGED:
            break;

        case NOTIFICATION_PROCESS:
        case NOTIFICATION_INTERNAL_PROCESS:
            _process();
            break;
    }
}

static constexpr auto patches = 2;

static constexpr std::array<float, HeightMap::m_mesh_cache_size> lod_distance = { HeightMap::chunk_size * patches,
    HeightMap::chunk_size *patches * 2,
    HeightMap::chunk_size *patches * 3,
    HeightMap::chunk_size *patches * 4,
    HeightMap::chunk_size *patches * 5,
    HeightMap::chunk_size *patches * 6,
    HeightMap::chunk_size *patches * 7 };

template <uint8_t Start = 0, uint8_t End = HeightMap::m_mesh_cache_size>
void static_iterate(const Vector2 &rel_pos_camera, entt::registry &registry, const entt::entity &entity)
{
    if constexpr (Start < End)
    {
        const auto &size = lod_distance[Start];
        if (rel_pos_camera.x < size && rel_pos_camera.y < size)
        {
            registry.emplace<HeightMap::Lod<Start>>(entity);
            return;
        }

        static_iterate<Start + 1, End>(rel_pos_camera, registry, entity);
    }
}

void HeightMap::set_lod_by_distance(const Vector3 &viewer_pos)
{
    //print_line(String("Camera pos = ") + viewer_pos.operator String());

    const auto view = m_registry.view<OffsetChunk>();

    view.each([&](auto entity, const OffsetChunk &offset) {
        const Chunk &chunk = m_offset_chunks[offset.m_value];

        Vector2 pos = Vector2{ chunk.x, chunk.z };
        Vector2 rel_pos_camera = Vector2{ viewer_pos.x, viewer_pos.z } - pos;
        rel_pos_camera = rel_pos_camera.abs();

        if (rel_pos_camera.x >= lod_distance.back() || rel_pos_camera.y >= lod_distance.back())
        {
            m_registry.emplace<Lod<m_mesh_cache_size - 1>>(entity);
            return;
        }

        static_iterate(rel_pos_camera, m_registry, entity);
    });
}

void HeightMap::set_mesh_by_lod()
{
    Transform transform{ Basis{} };
    for_constexpr<m_mesh_cache_size>([&](auto index) {
        DirectMultiMeshInstance &multimesh_instance = m_mesh_cache[index];

        const auto view = m_registry.view<OffsetChunk, Lod<index>>();

        auto instance_count = view.size_hint();
        multimesh_instance.set_instance_count(instance_count);

        uint32_t i = 0;

        view.each([&](const OffsetChunk &offset) {
            const Chunk &chunk = m_offset_chunks[offset.m_value];
            transform.origin = Vector3{ chunk.x, 0, chunk.z };
            multimesh_instance.set_instance_transform(i, transform);
            multimesh_instance.set_instance_lod(i, index);
            ++i;
        });
    });
}

void HeightMap::clear_lods()
{
    /*
    {
        const auto view = m_registry.view<OffsetChunk>();

        view.each([&](auto entity, auto) {
            std::array<bool, 7> result{};

            for_constexpr<m_mesh_cache_size>([&](auto index) {
                result[index] = m_registry.has<Lod<index>>(entity);
            });

            auto count_true = std::count(result.begin(), result.end(), true);

            assert(count_true <= 1);
        });
    }
    */

    for_constexpr<m_mesh_cache_size>([&](auto index) {
        m_registry.clear<Lod<index>>();
    });
}

void HeightMap::_process()
{

    {
        draw_triangles();
    }

    if (is_inside_tree())
    {
        Node *camera_node = get_child(0);

        assert(camera_node != nullptr);

        Vector3 camera_pos = Object::cast_to<Camera>(camera_node)->get_global_transform().origin;

        if (m_custom_material.is_valid())
        {
            m_custom_material->set_shader_param("cam_position", camera_pos);
        }
    }

    // Get viewer pos
    Vector3 viewer_pos = _manual_viewer_pos;
    Viewport *viewport = get_viewport();
    if (viewport != nullptr)
    {
        Camera *camera = viewport->get_camera();
        if (camera != nullptr)
        {
            viewer_pos = camera->get_global_transform().origin;
        }
    }

    set_lod_by_distance(viewer_pos);

    set_mesh_by_lod();

    clear_lods();
}

void HeightMap::draw_triangles()
{
    if (m_data.is_null() || m_data->m_heights.empty())
    {
        return;
    }

    if (m_immediateGeom != nullptr)
    {
        return;
    }

    m_immediateGeom = memnew(ImmediateGeometry);

    add_child(m_immediateGeom);

    SpatialMaterial *line_material = memnew(SpatialMaterial);
    line_material->set_flag(SpatialMaterial::FLAG_UNSHADED, true);
    line_material->set_line_width(3.0);
    line_material->set_feature(SpatialMaterial::FEATURE_TRANSPARENT, true);
    line_material->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR, true);
    line_material->set_flag(SpatialMaterial::FLAG_SRGB_VERTEX_COLOR, true);
    line_material->set_albedo(Color(1, 1, 1));

    m_immediateGeom->set_material_override(line_material);

    m_immediateGeom->begin(Mesh::PRIMITIVE_LINES);
    m_immediateGeom->set_color(Color{ 1, 0, 0 });

    constexpr uint32_t limit = 50;
    constexpr uint32_t offset = 100;

    const auto res = m_data->get_resolution();

    constexpr float chunk_size = 0.5;

    constexpr std::array<std::pair<uint8_t, uint8_t>, 5> combination{
        std::pair{ 0, 1 },
        std::pair{ 1, 2 },
        std::pair{ 2, 0 },
        std::pair{ 2, 3 },
        std::pair{ 1, 3 }
    };

    for (uint32_t y{ 0 }; y < limit; ++y)
    {
        for (uint32_t x{ 0 }; x < limit; ++x)
        {
            const auto col = x + offset;
            const auto row = y + offset;
            auto local_x = col * chunk_size, local_y = row * chunk_size;
            auto index = col + row * res;
            auto &heights = m_data->m_heights;

            const std::array<Vector3, 4> vertices{
                Vector3{ local_x, heights[index], local_y },
                Vector3{ local_x + chunk_size, heights[index + 1], local_y },
                Vector3{ local_x, heights[index + res], local_y + chunk_size },
                Vector3{ local_x + chunk_size, heights[index + 1 + res], local_y + chunk_size }
            };

            auto pos = Object::cast_to<Spatial>(this->get_parent())->get_global_transform().get_origin();

            for (auto &&pair : combination)
            {
                m_immediateGeom->add_vertex(vertices[pair.first] + pos);
                m_immediateGeom->add_vertex(vertices[pair.second] + pos);
            }
        }
    }

    m_immediateGeom->end();
}

Point2i HeightMap::local_pos_to_cell(Vector3 local_pos) const
{
    return Point2i(
            static_cast<int>(local_pos.x),
            static_cast<int>(local_pos.z));
}

void HeightMap::set_custom_material(Ref<ShaderMaterial> p_material)
{
    if (m_custom_material != p_material)
    {
        m_custom_material = p_material;

        for (auto &&chunk : m_mesh_cache)
        {
            chunk.set_material(m_custom_material);
        }
    }
}

void HeightMap::_bind_methods()
{

    ClassDB::bind_method(D_METHOD("get_data"), &HeightMap::get_data);
    ClassDB::bind_method(D_METHOD("set_data", "data"), &HeightMap::set_data);

    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "data", PROPERTY_HINT_RESOURCE_TYPE, "HeightMapData"), "set_data", "get_data");

    ClassDB::bind_method(D_METHOD("get_custom_material"), &HeightMap::get_custom_material);
    ClassDB::bind_method(D_METHOD("set_custom_material", "material"), &HeightMap::set_custom_material);

    ClassDB::add_property(get_class_static(),
            PropertyInfo(Variant::OBJECT, "custom_material", PROPERTY_HINT_RESOURCE_TYPE, "ShaderMaterial"),
            _scs_create("set_custom_material"),
            _scs_create("get_custom_material"));
}
