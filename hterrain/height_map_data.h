#pragma once

#include <core/resource.h>

#include "scene/resources/texture.h"

#include <vector>

class HeightMapData : public Resource {
	GDCLASS(HeightMapData, Resource)
public:
    HeightMapData();

    std::vector<float> m_heights;

    uint16_t m_multiplier = 100;
    uint8_t m_depth = 32;

    void set_multiplier(uint16_t p_multiplier);
    uint16_t get_multiplier() { return m_multiplier; }

	void set_depth(uint8_t p_depth);
	uint8_t get_depth() { return m_depth; }

    void set_resolution(uint32_t p_res);
    uint32_t get_resolution() const;

    void set_texture(Ref<Texture> p_texture);
    inline Ref<Texture> get_texture()
    {
        return m_texture;
    }

private:
    static void _bind_methods();

private:
    Ref<Texture> m_texture;

    uint32_t m_resolution;
};

