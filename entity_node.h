#pragma once

#include <scene/3d/spatial.h>

class MeshInstance;

enum class Type : uint8_t
{
    None,
    Capsule,
    Box,
    Triangle,
    TriangleMesh
};

class EntityNode : public Spatial
{
    GDCLASS(EntityNode, Spatial)
public:

    EntityNode();
    virtual ~EntityNode()
    {
    }

    static void _bind_methods();

    void set_mesh(Ref<Mesh> p_mesh);
    Ref<ArrayMesh> get_mesh();

    void set_type(uint8_t p_type);

    inline uint8_t get_type()
    {
        return static_cast<uint8_t>(m_type);
    }

    void set_offset(uint32_t p_offset);
    uint32_t get_offset()
    {
        return m_offset;
    }

    uint32_t m_offset{ 0 };

    MeshInstance *m_mesh;
    Type m_type = Type::None;
};
