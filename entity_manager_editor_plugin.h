#pragma once

#include "editor/editor_plugin.h"

#include "editor/editor_node.h"

class EntityManager;

class EntityManagerEditorPlugin : public EditorPlugin
{
    GDCLASS(EntityManagerEditorPlugin, EditorPlugin)
public:
    EntityManagerEditorPlugin(EditorNode *p_editor);
    ~EntityManagerEditorPlugin();

    virtual bool forward_spatial_gui_input(Camera *p_camera, const Ref<InputEvent> &p_event);
    virtual String get_name() const { return "EntityManager"; }
    bool has_main_screen() const { return false; }
    virtual void edit(Object *p_object);
    virtual bool handles(Object *p_object) const;
    virtual void make_visible(bool p_visible);

protected:
	static void _bind_methods();

private:
    void _entity_manager_exited_scene();
    void _menu_item_selected(int id);
	void _import_file_selected(String p_path);

	void _import_raw_file_selected(String path);

    void replace_nodes();
    void remove_entities();
    void get_terrain();

private:
	enum MenuItems {
		MENU_IMPORT_RAW = 0
	};

	EditorNode *_editor;
    EntityManager *m_entity_manager;
    HBoxContainer *_toolbar;

    bool _mouse_pressed;
};
