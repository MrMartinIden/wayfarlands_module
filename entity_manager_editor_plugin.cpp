#include "entity_manager_editor_plugin.h"

#include "entity_manager.h"

#include <core/os/input.h>
#include <editor/editor_scale.h>
#include <scene/3d/camera.h>
#include <scene/gui/color_rect.h>
#include <scene/scene_string_names.h>


inline Ref<Texture> get_icon(String name) {
	return EditorNode::get_singleton()->get_gui_base()->get_icon(name, "EditorIcons");
}

EntityManagerEditorPlugin::EntityManagerEditorPlugin(EditorNode *p_editor)
{
    _editor = p_editor;
    _mouse_pressed = false;
    //_height_map = NULL;

    _toolbar = memnew(HBoxContainer);
    add_control_to_container(CONTAINER_SPATIAL_EDITOR_MENU, _toolbar);
    _toolbar->hide();

    {
        Button *button = memnew(Button);
        button->set_text(TTR("Replace nodes"));
        button->connect("pressed", this, "replace_nodes");
        _toolbar->add_child(button);
    }

    _toolbar->add_child(memnew(VSeparator));

    {
        Button *button = memnew(Button);
        button->set_text(TTR("Remove entities"));
        button->connect("pressed", this, "remove_entities");
        _toolbar->add_child(button);
    }

    _toolbar->add_child(memnew(VSeparator));

    {
        Button *button = memnew(Button);
        button->set_text(TTR("Get terrain"));
        button->connect("pressed", this, "get_terrain");
        _toolbar->add_child(button);
    }
}

EntityManagerEditorPlugin::~EntityManagerEditorPlugin()
{
}

void EntityManagerEditorPlugin::replace_nodes()
{
    m_entity_manager->replace_nodes();
}

void EntityManagerEditorPlugin::remove_entities()
{
    m_entity_manager->remove_entities();
}

void EntityManagerEditorPlugin::get_terrain()
{
    m_entity_manager->get_terrain();
}

bool EntityManagerEditorPlugin::forward_spatial_gui_input(Camera *p_camera, const Ref<InputEvent> &p_event)
{

    if (m_entity_manager == nullptr)
        return false;

    bool captured_event = false;

    return captured_event;
}

void EntityManagerEditorPlugin::edit(Object *p_object)
{

    //printf("Edit %i\n", p_object);
    EntityManager *node = p_object ? Object::cast_to<EntityManager>(p_object) : NULL;

    if (m_entity_manager)
    {
        m_entity_manager->disconnect(SceneStringNames::get_singleton()->tree_exited, this, "_entity_manager_exited_scene");
    }

    m_entity_manager = node;

    if (m_entity_manager)
    {
        m_entity_manager->connect(SceneStringNames::get_singleton()->tree_exited, this, "_entity_manager_exited_scene");
    }
}

void EntityManagerEditorPlugin::_entity_manager_exited_scene()
{
    //print_line("HeightMap exited scene");
    edit(NULL);
}

bool EntityManagerEditorPlugin::handles(Object *p_object) const
{
    return p_object->is_class("EntityManager");
}

void EntityManagerEditorPlugin::make_visible(bool p_visible)
{
    _toolbar->set_visible(p_visible);
}

void EntityManagerEditorPlugin::_bind_methods()
{

    ClassDB::bind_method(D_METHOD("_entity_manager_exited_scene"), &EntityManagerEditorPlugin::_entity_manager_exited_scene);
    ClassDB::bind_method(D_METHOD("replace_nodes"), &EntityManagerEditorPlugin::replace_nodes);
    ClassDB::bind_method(D_METHOD("remove_entities"), &EntityManagerEditorPlugin::remove_entities);
    ClassDB::bind_method(D_METHOD("get_terrain"), &EntityManagerEditorPlugin::get_terrain);
}
